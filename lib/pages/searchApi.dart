import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;

import 'IssueDescription.dart';
import 'UserProfile.dart';

class User {
  final String userId;
  final String avatarUrl;
  final String name;
  final String username;

  User({
    this.userId,
    this.avatarUrl,
    this.name,
    this.username,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['id'].toString(),
      avatarUrl: json['avatar_url'].toString(),
      name: json['name'].toString(),
      username: json['username'].toString(),
    );
  }
}

class Mr {
  Mr({
    this.id,
    this.iid,
    this.projectId,
    this.title,
    this.description,
  });
  int id;
  int iid;
  int projectId;
  String title;
  String description;

  factory Mr.fromJson(Map<String, dynamic> json) => Mr(
        id: json["id"],
        iid: json["iid"],
        projectId: json["project_id"],
        title: json["title"],
        description: json["description"],
      );
}

class Project {
  final String projectId;
  final String projectAvatarUrl;
  final String projectName;
  final String pathWithNamespace;
  final String readmeUrl;

  Project(
      {this.projectId,
      this.projectAvatarUrl,
      this.projectName,
      this.pathWithNamespace,
      this.readmeUrl});

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      projectId: json['id'].toString(),
      projectAvatarUrl: json['avatar_url'].toString(),
      projectName: json['name'].toString(),
      pathWithNamespace: json['path_with_namespace'].toString(),
      readmeUrl: json['readme_url'].toString(),
    );
  }
}

class searchApi extends StatefulWidget {
  @override
  _searchApiState createState() => _searchApiState();
  String token;
  String Query;
  List SearchZone = [];
  searchApi({this.Query, this.token, this.SearchZone});
}

class _searchApiState extends State<searchApi> {
  @override
  List<Issue> issueList = new List<Issue>();
  List<User> userList = new List<User>();
  List<Project> projectList = new List<Project>();
  List<Mr> mrList = new List<Mr>();
  List<String> list;
  Future<void> getIssue(String query, String token) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/search?scope=issues&search=$query&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            issueList.add(Issue.fromJson(map));
            //print(issueList);
          }
        }
      }
    } else {
      //print(issueList);
      throw Exception('Failed to load post');
    }
  }

  Future<void> getMr(String query, String token) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/search?scope=merge_requests&search=$query&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    //print(response.statusCode);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });
      //print(values);
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];

            mrList.add(Mr.fromJson(map));
          }
        }
      }
      //print(mrList);
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<void> getUser(String query, String token) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/search?scope=users&search=$query&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            userList.add(User.fromJson(map));
          }
        }
      }
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<void> getProject(String query, String token) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/search?scope=projects&search=$query&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            projectList.add(Project.fromJson(map));
          }
        }
      }
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.SearchZone[0] == true) {
      this.getIssue(widget.Query, widget.token);
      issueList.sort();
    } else if (widget.SearchZone[1] == true) {
      this.getMr(widget.Query, widget.token);
      mrList.sort();
    } else if (widget.SearchZone[2] == true) {
      this.getUser(widget.Query, widget.token);
      userList.sort();
    } else {
      this.getProject(widget.Query, widget.token);
      projectList.sort();
    }
  }

  Widget build(BuildContext context) {
    if (widget.SearchZone[0] == true) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Issues: " + widget.Query),
            // actions: <Widget>[
            //   IconButton(
            //     onPressed: () {
            //       showSearch(
            //           context: context,
            //           delegate: IssueSearch(issueList, widget.token));
            //     },
            //     icon: Icon(Icons.search),
            //   )
            // ],
          ),
          body: new Container(
              margin: EdgeInsets.all(8),
              child: new Column(
                children: <Widget>[createListViewIssue(issueList)],
              )));
    } else if (widget.SearchZone[1] == true) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Merge request: " + widget.Query),
            // actions: <Widget>[
            //   IconButton(
            //     onPressed: () {
            //       showSearch(
            //           context: context,
            //           delegate: IssueSearch(mrList, widget.token));
            //     },
            //     icon: Icon(Icons.search),
            //   )
            // ],
          ),
          body: new Container(
              margin: EdgeInsets.all(8),
              child: new Column(
                children: <Widget>[createListViewMr(mrList)],
              )));
    } else if (widget.SearchZone[2] == true) {
      return Scaffold(
        body: new Container(
          margin: EdgeInsets.all(8),
          child: new Column(
            children: <Widget>[
              SizedBox(
                height: 30.0,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      "Users: " + "\"" + widget.Query + "\"",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 21.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              createListViewUSer(userList),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: new Container(
          margin: EdgeInsets.all(8),
          child: new Column(
            children: <Widget>[
              SizedBox(
                height: 30.0,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      "Projects: " + "\"" + widget.Query + "\"",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 21.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              createListViewProjects(projectList),
            ],
          ),
        ),
      );
    }
  }

  Widget createListViewIssue(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(listVariable[index].issueState),
                    subtitle: RichText(
                      text: TextSpan(
                        text: '\n',
                        style: new TextStyle(
                          fontSize: 5.0,
                          color: Colors.grey,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: "Project ID: ",
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey,
                            ),
                          ),
                          TextSpan(
                            text: listVariable[index].projectId + "\n",
                            style:
                                TextStyle(fontSize: 12.5, color: Colors.orange),
                          ),
                          TextSpan(
                            text: "\n",
                            style:
                                TextStyle(fontSize: 0.0, color: Colors.orange),
                          ),
                          TextSpan(
                            text: listVariable[index].issueDescription,
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey[400],
                            ),
                          ),
                        ],
                      ),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 15,
                      color: Colors.grey[400],
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => IssueDescription(
                              token: widget.token,
                              issueId: listVariable[index].issueId
                              //issueId: _issueList[index].issueId,
                              ),
                        ),
                      );
                    },
                  ),
                  Divider(
                    color: Colors.grey[300],
                    endIndent: 0.0,
                    indent: 80.0,
                  )
                ],
              ));
        },
      ),
    );
  }

  createListViewProjects(List<Project> projectList) {
    return Flexible(
      child: new ListView.builder(
        itemCount: projectList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(projectList[index].projectName),
                  subtitle: RichText(
                    text: TextSpan(
                      text: '\n',
                      style: new TextStyle(
                        fontSize: 5.0,
                        color: Colors.grey,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Project ID: ",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: projectList[index].projectId + "\n",
                          style:
                              TextStyle(fontSize: 12.5, color: Colors.orange),
                        ),
                        TextSpan(
                          text: "\n",
                          style: TextStyle(fontSize: 0.0, color: Colors.orange),
                        ),
                        TextSpan(
                          text: projectList[index].pathWithNamespace,
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[400],
                          ),
                        ),
                      ],
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                    color: Colors.grey[400],
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProjectDescription(
                          token: widget.token,
                          projectId: projectList[index].projectId,
                          projectName: projectList[index].projectName,
                          projectAvatar: projectList[index].projectAvatarUrl,
                          readmeUrl: projectList[index].readmeUrl,
                          //issueId: _issueList[index].issueId,
                        ),
                      ),
                    );
                  },
                ),
                Divider(
                  color: Colors.grey[300],
                  endIndent: 0.0,
                  indent: 80.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  createListViewUSer(List<User> userList) {
    return Flexible(
      child: new ListView.builder(
        itemCount: userList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(userList[index].name),
                    subtitle: RichText(
                      text: TextSpan(
                        text: '\n',
                        style: new TextStyle(
                          fontSize: 5.0,
                          color: Colors.grey,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: "(" + userList[index].userId + ")",
                            style:
                                TextStyle(fontSize: 12.5, color: Colors.orange),
                          ),
                          TextSpan(
                            text: "\n",
                            style:
                                TextStyle(fontSize: 12.5, color: Colors.orange),
                          ),
                        ],
                      ),
                    ),
                    trailing: Text(
                      userList[index].username,
                      style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.blue,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UserProfile(
                            token: widget.token,
                            name: userList[index].name,
                            userName: userList[index].username,
                            userAvatar: userList[index].avatarUrl,
                            userId: userList[index].userId,
                            //issueId: _issueList[index].issueId
                            //issueId: _issueList[index].issueId,
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ));
        },
      ),
    );
  }

  createListViewMr(List<Mr> mrList) {
    return Flexible(
      child: new ListView.builder(
        itemCount: mrList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(mrList[index].title),
                    subtitle: RichText(
                      text: TextSpan(
                        text: '\n',
                        style: new TextStyle(
                          fontSize: 5.0,
                          color: Colors.grey,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: "Project ID: ",
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey,
                            ),
                          ),
                          TextSpan(
                            text: mrList[index].projectId.toString() + '\n',
                            style:
                                TextStyle(fontSize: 12.5, color: Colors.orange),
                          ),
                          TextSpan(
                            text: "\n",
                            style:
                                TextStyle(fontSize: 0.0, color: Colors.orange),
                          ),
                          TextSpan(
                            text: mrList[index].description,
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey[400],
                            ),
                          ),
                        ],
                      ),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 15,
                      color: Colors.grey[400],
                    ),
                    onTap: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => IssueDescription(
                      //         token: widget.token,
                      //         issueId: _issueList[index].issueId
                      //         //issueId: _issueList[index].issueId,
                      //         ),
                      //   ),
                      // );
                    },
                  ),
                  Divider(
                    color: Colors.grey[300],
                    endIndent: 0.0,
                    indent: 80.0,
                  )
                ],
              ));
        },
      ),
    );
  }
}
