import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/mergeRequestDescription.dart';
import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:gitlab_mate/pages/ClosedIssueList.dart';
import 'IssueDescription.dart';
import 'package:gitlab_mate/pages/CreateIssue.dart';
import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:gitlab_mate/pages/projectIssue.dart';

class MergeRequest {
  String id;
  String iid;
  String projectId;
  String title;
  String description;
  String state;
  String createdAt;
  String updatedAt;
  //MergedBy mergedBy;
  String mergedAt;

  // String targetBranch;
  // String sourceBranch;
  // int userNotesCount;
  // int upvotes;
  // int downvotes;
  // MergedBy author;
  //List<Null> assignees;

  MergeRequest({
    this.id,
    this.iid,
    this.projectId,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    //this.mergedBy,
    this.mergedAt,
  });

  factory MergeRequest.fromJson(Map<String, dynamic> json) {
    return MergeRequest(
      id: json['id'].toString(),
      iid: json['iid'].toString(),
      projectId: json['project_id'].toString(),
      title: json['title'].toString(),
      description: json['description'].toString(),
      state: json['state'].toString(),
      createdAt: json['created_at'].toString(),
      updatedAt: json['updated_at'].toString(),
      mergedAt: json['merged_at'].toString(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    //data['iid'] = this.iid;
    data['project_id'] = this.projectId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['state'] = this.state;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['merged_at'] = this.mergedAt;
    return data;
  }
}

class ProjectMergeRequest extends StatefulWidget {
  final String token;
  final String projectId;
  final String title;

  ProjectMergeRequest({this.token, this.projectId, this.title});
  @override
  _ProjectMRState createState() => _ProjectMRState();
}

class _ProjectMRState extends State<ProjectMergeRequest> {
  List<MergeRequest> _projectMRList = new List<MergeRequest>();
  Future<List<MergeRequest>> _fetchProjectIssue(
      String token, String projectId) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/projects/$projectId/merge_requests?private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _projectMRList.add(MergeRequest.fromJson(map));
          }
        }
      }
      return _projectMRList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchProjectIssue(widget.token, widget.projectId);
    _projectMRList.sort();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      "Merge Requests",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate: ProjectIssueSearch(_projectMRList, widget.token),
                  );
                },
                child: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                // ListTile(
                //   leading: Icon(Icons.add),
                //   title: Text(
                //     "Create new issue!",
                //     style: TextStyle(
                //       color: Colors.black,
                //       fontWeight: FontWeight.w400,
                //       fontSize: 14.0,
                //     ),
                //   ),
                //   onTap: () {
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //         builder: (context) => CreateIssue(
                //           token: widget.token,
                //           title: widget.title,
                //           projectId: widget.projectId,
                //         ),
                //       ),
                //     );
                //   },
                // ),
              ],
            ),
          ),
          createListView(_projectMRList),
        ],
      ),
    );
  }

  Widget createListView(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  title: RichText(
                    text: TextSpan(
                      text: listVariable[index].title,
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: " (!" + listVariable[index].iid + ")",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                  ),
                  subtitle: RichText(
                    text: TextSpan(
                      text: '\n',
                      style: new TextStyle(
                        fontSize: 5.0,
                        color: Colors.grey,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Status: ",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: listVariable[index].state.toUpperCase() + "\n",
                          style: TextStyle(fontSize: 12.5, color: Colors.green),
                        ),
                      ],
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                    color: Colors.grey[400],
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MergeRequestDescription(
                          token: widget.token,
                          mrId: _projectMRList[index].id,
                          mrIid: _projectMRList[index].iid,
                          title: _projectMRList[index].title,
                          mrDescription: _projectMRList[index].description,
                          mrState: _projectMRList[index].state,
                          projectId: _projectMRList[index].projectId,
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ProjectIssueSearch extends SearchDelegate<MergeRequest> {
  final List<MergeRequest> projectIssueList;
  static MergeRequest result;
  final String token;
  ProjectIssueSearch(this.projectIssueList, this.token);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? projectIssueList
        : projectIssueList
            .where((p) => p.title.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No results found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                title: Text(showList.title),
                subtitle: RichText(
                  text: TextSpan(
                    text: '\n',
                    style: new TextStyle(
                      fontSize: 5.0,
                      color: Colors.grey,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "!" + showList.id + "\n",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      TextSpan(
                        text: "\n",
                        style: TextStyle(fontSize: 0.0, color: Colors.orange),
                      ),
                      TextSpan(
                        text: "Status: ",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey,
                        ),
                      ),
                      TextSpan(
                        text: showList.state.toUpperCase() + "\n",
                        style: TextStyle(fontSize: 12.5, color: Colors.green),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }
}
