import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/CreateNote.dart';
import 'package:http/http.dart' as http;

class MergeRequest {
  final String issueId;
  final String projectId;
  final String issueTitle;
  final String mergeRequestDescription;
  final String issueState;
  final String projectAvatarUrl;
  MergeRequest(
      {this.mergeRequestDescription,
      this.issueId,
      this.issueState,
      this.issueTitle,
      this.projectId,
      this.projectAvatarUrl});

  factory MergeRequest.fromJson(Map<String, dynamic> json) {
    return MergeRequest(
        projectId: json['project_id'].toString(),
        issueId: json['id'].toString(),
        mergeRequestDescription: json['description'].toString(),
        issueState: json['state'].toString(),
        issueTitle: json['title'].toString(),
        projectAvatarUrl: json['author.avatar_url'].toString());
  }
}

Notes notesFromJson(String str) => Notes.fromJson(json.decode(str));

class Notes {
  int id;
  //Null type;
  String body;
  //Null attachment;
  Author author;
  String createdAt;
  String updatedAt;
  bool system;
  int noteableId;
  String noteableType;
  bool resolvable;
  bool confidential;
  int noteableIid;
  CommandsChanges commandsChanges;

  Notes(
      {this.id,
      //this.type,
      this.body,
      //this.attachment,
      this.author,
      this.createdAt,
      this.updatedAt,
      this.system,
      this.noteableId,
      this.noteableType,
      this.resolvable,
      this.confidential,
      this.noteableIid,
      this.commandsChanges});

  Notes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    //type = json['type'];
    body = json['body'];
    //attachment = json['attachment'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    system = json['system'];
    noteableId = json['noteable_id'];
    noteableType = json['noteable_type'];
    resolvable = json['resolvable'];
    confidential = json['confidential'];
    noteableIid = json['noteable_iid'];
    commandsChanges = json['commands_changes'] != null
        ? new CommandsChanges.fromJson(json['commands_changes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    //data['type'] = this.type;
    data['body'] = this.body;
    //data['attachment'] = this.attachment;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['system'] = this.system;
    data['noteable_id'] = this.noteableId;
    data['noteable_type'] = this.noteableType;
    data['resolvable'] = this.resolvable;
    data['confidential'] = this.confidential;
    data['noteable_iid'] = this.noteableIid;
    if (this.commandsChanges != null) {
      data['commands_changes'] = this.commandsChanges.toJson();
    }
    return data;
  }
}

class Author {
  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  Author(
      {this.id,
      this.name,
      this.username,
      this.state,
      this.avatarUrl,
      this.webUrl});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['state'] = this.state;
    data['avatar_url'] = this.avatarUrl;
    data['web_url'] = this.webUrl;
    return data;
  }
}

class MergeRequestDescription extends StatefulWidget {
  final String token;
  final String mrId;
  final String mrIid;
  final String title;
  final String mrDescription;
  final String mrState;
  final String projectId;

  MergeRequestDescription(
      {this.mrId,
      this.mrIid,
      this.token,
      this.title,
      this.mrDescription,
      this.mrState,
      this.projectId});
  @override
  _MergeRequestDescriptionState createState() =>
      _MergeRequestDescriptionState();
}

class _MergeRequestDescriptionState extends State<MergeRequestDescription> {
  get mergeRequestDescription => null;

  Future<MergeRequest> _futureMergeRequest;
  Future<MergeRequest> _fetchMergeRequest(
      String token, String projectId, String mrIid) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4//projects/$projectId/merge_requests/$mrIid&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    //print(response.body);
    if (response.statusCode == 200) {
      final jsonresponse = json.decode(response.body);
      return MergeRequest.fromJson(jsonresponse[0]);
    } else {
      throw Exception('Failed to load Issue');
    }
  }

  List<Notes> _noteList = new List<Notes>();
  Notes _addNotes;
  Future<List<Notes>> _fetchNotes(
      String token, String projectId, String mrIid) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/projects/$projectId/merge_requests/$mrIid/notes?sort=asc&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _noteList.add(Notes.fromJson(map));
          }
        }
      }
      return _noteList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<Notes> _createNote(
      String token, String projectId, String body, String mrIid) async {
    final postsURL =
        'https://gitlab.com/api/v4/projects/$projectId/merge_requests/$mrIid/notes?&private_token=$token';
    final response = await http.post(postsURL, body: {"body": body});
    if (response.statusCode == 201) {
      final String responseString = response.body;
      return notesFromJson(responseString);
    } else {
      return null;
    }
  }

  Future<int> _acceptMergeRequest(
      String token, String projectId, String mrIid) async {
    final response = await http.post(
        'https://gitlab.com/api/v4/projects/$projectId/merge_requests/$mrIid/merge?private_token=$token');
    if (response.statusCode == 201) {
      //return Project.fromJson(jsonDecode(response.body));
      return 0;
    } else {
      return 1;
    }
  }

  final TextEditingController noteBody = TextEditingController();

  @override
  void initState() {
    super.initState();
    _futureMergeRequest =
        _fetchMergeRequest(widget.token, widget.projectId, widget.mrIid);
    this._fetchNotes(widget.token, widget.projectId, widget.mrIid);
    //print(_futureIssue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30.0,
              ),
              ListTile(
                title: RichText(
                  text: TextSpan(
                    text: widget.title,
                    style: new TextStyle(
                      fontSize: 21.0,
                      color: Colors.blue[800],
                      fontWeight: FontWeight.w500,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: " (" + widget.mrIid + ")",
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.grey[500],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 15.0,
                    ),
                    ListTile(
                      title: Text(
                        widget.mrDescription.length != 0
                            ? widget.mrDescription
                            : "No Issue Description",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w300,
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              ),
              /*
            Flexible(
              child: FutureBuilder<Issue>(
                future: _futureIssue,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 15.0,
                          ),
                          ListTile(
                            title: Text(
                              snapshot.data.MergeRequestDescription.length != 0
                                  ? snapshot.data.MergeRequestDescription
                                  : "No Issue Description",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Center(
                            child: CircularProgressIndicator(),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            */
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Text(
                    "Comments",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              createListView(_noteList),
              SizedBox(
                height: 15.0,
              ),
              TextField(
                controller: noteBody,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueGrey),
                  ),
                  hintText: "type your comment here",
                  filled: true,
                  fillColor: Colors.grey[200],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.add),
                      title: Text(
                        "Add new comments",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontSize: 14.0,
                        ),
                      ),
                      onTap: () async {
                        final String body = noteBody.text;
                        // final String label = issueLabel.text;
                        final Notes mrNote = await _createNote(
                            widget.token, widget.projectId, body, widget.mrIid);
                        noteBody.clear();
                        setState(
                          () {
                            _addNotes = mrNote;
                            _fetchNotes(
                                widget.token, widget.projectId, widget.mrIid);
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                onTap: () async {
                  await _acceptMergeRequest(
                      widget.token, widget.projectId, widget.mrIid);
                  Navigator.pop(context);
                },
                child: Padding(
                  padding: EdgeInsets.only(bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 27),
                        decoration: BoxDecoration(
                          color: Colors.green.withOpacity(.91),
                          borderRadius: BorderRadius.circular(27),
                        ),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Accept Merge Request",
                              style: new TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                            //SizedBox(width: 30),
                            /*
                            Icon(
                              Icons.power_settings_new,
                              color: Colors.white,
                            ),
                            */
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget createListView(List<Notes> listVariable) {
    return Container(
      child: new ListView.builder(
        primary: false,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 18.0,
                    ),
                    Flexible(
                      child: Container(
                        child: ClipRRect(
                          borderRadius:
                              BorderRadius.all(Radius.circular(100.0)),
                          child: listVariable[index].author.avatarUrl != null
                              ? CachedNetworkImage(
                                  width: 18.0,
                                  height: 18.0,
                                  fit: BoxFit.cover,
                                  imageUrl:
                                      listVariable[index].author.avatarUrl,
                                  placeholder: (context, url) =>
                                      new CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Colors.yellow[700]),
                                  ),
                                  errorWidget: (context, url, error) => Image(
                                    height: 18.0,
                                    width: 18.0,
                                    image: AssetImage(
                                        'assets/icons/gitlab-mate-yellow-icon.png'),
                                  ),
                                )
                              : Image(
                                  height: 18.0,
                                  width: 18.0,
                                  image: AssetImage(
                                    'assets/icons/gitlab-mate-yellow-icon.png',
                                  ),
                                ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          listVariable[index].author.name,
                          style: TextStyle(
                            color: Colors.blueGrey,
                            fontSize: 13.0,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 18.0,
                    ),
                    Text(
                      "at: ",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 11.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "(" + listVariable[index].createdAt + ")",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 11.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Text(
                      " says :",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 11.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
                Divider(),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 18.0,
                    ),
                    Flexible(
                      child: Text(
                        listVariable[index].body,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
