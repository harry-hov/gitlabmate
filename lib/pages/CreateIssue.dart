import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// To parse this JSON data, do
//
//     final issue = issueFromJson(jsonString);

Issue issueFromJson(String str) => Issue.fromJson(json.decode(str));

String issueToJson(Issue data) => json.encode(data.toJson());

class Issue {
  Issue({
    this.id,
    this.iid,
    this.projectId,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.closedAt,
    this.closedBy,
    this.labels,
    this.milestone,
    this.assignees,
    this.author,
    this.assignee,
    this.userNotesCount,
    this.mergeRequestsCount,
    this.upvotes,
    this.downvotes,
    this.dueDate,
    this.confidential,
    this.discussionLocked,
    this.webUrl,
    this.timeStats,
    this.taskCompletionStatus,
    this.blockingIssuesCount,
    this.hasTasks,
    this.links,
    this.references,
    this.subscribed,
    this.movedToId,
    this.serviceDeskReplyTo,
  });

  int id;
  int iid;
  int projectId;
  String title;
  dynamic description;
  String state;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic closedAt;
  dynamic closedBy;
  List<String> labels;
  dynamic milestone;
  List<dynamic> assignees;
  Author author;
  dynamic assignee;
  int userNotesCount;
  int mergeRequestsCount;
  int upvotes;
  int downvotes;
  dynamic dueDate;
  bool confidential;
  dynamic discussionLocked;
  String webUrl;
  TimeStats timeStats;
  TaskCompletionStatus taskCompletionStatus;
  int blockingIssuesCount;
  bool hasTasks;
  Links links;
  References references;
  bool subscribed;
  dynamic movedToId;
  dynamic serviceDeskReplyTo;

  factory Issue.fromJson(Map<String, dynamic> json) => Issue(
        id: json["id"],
        iid: json["iid"],
        projectId: json["project_id"],
        title: json["title"],
        description: json["description"],
        state: json["state"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        closedAt: json["closed_at"],
        closedBy: json["closed_by"],
        labels: List<String>.from(json["labels"].map((x) => x)),
        milestone: json["milestone"],
        assignees: List<dynamic>.from(json["assignees"].map((x) => x)),
        author: Author.fromJson(json["author"]),
        assignee: json["assignee"],
        userNotesCount: json["user_notes_count"],
        mergeRequestsCount: json["merge_requests_count"],
        upvotes: json["upvotes"],
        downvotes: json["downvotes"],
        dueDate: json["due_date"],
        confidential: json["confidential"],
        discussionLocked: json["discussion_locked"],
        webUrl: json["web_url"],
        timeStats: TimeStats.fromJson(json["time_stats"]),
        taskCompletionStatus:
            TaskCompletionStatus.fromJson(json["task_completion_status"]),
        blockingIssuesCount: json["blocking_issues_count"],
        hasTasks: json["has_tasks"],
        links: Links.fromJson(json["_links"]),
        references: References.fromJson(json["references"]),
        subscribed: json["subscribed"],
        movedToId: json["moved_to_id"],
        serviceDeskReplyTo: json["service_desk_reply_to"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "iid": iid,
        "project_id": projectId,
        "title": title,
        "description": description,
        "state": state,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "closed_at": closedAt,
        "closed_by": closedBy,
        "labels": List<dynamic>.from(labels.map((x) => x)),
        "milestone": milestone,
        "assignees": List<dynamic>.from(assignees.map((x) => x)),
        "author": author.toJson(),
        "assignee": assignee,
        "user_notes_count": userNotesCount,
        "merge_requests_count": mergeRequestsCount,
        "upvotes": upvotes,
        "downvotes": downvotes,
        "due_date": dueDate,
        "confidential": confidential,
        "discussion_locked": discussionLocked,
        "web_url": webUrl,
        "time_stats": timeStats.toJson(),
        "task_completion_status": taskCompletionStatus.toJson(),
        "blocking_issues_count": blockingIssuesCount,
        "has_tasks": hasTasks,
        "_links": links.toJson(),
        "references": references.toJson(),
        "subscribed": subscribed,
        "moved_to_id": movedToId,
        "service_desk_reply_to": serviceDeskReplyTo,
      };
}

class Author {
  Author({
    this.id,
    this.name,
    this.username,
    this.state,
    this.avatarUrl,
    this.webUrl,
  });

  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  factory Author.fromJson(Map<String, dynamic> json) => Author(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        state: json["state"],
        avatarUrl: json["avatar_url"],
        webUrl: json["web_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "username": username,
        "state": state,
        "avatar_url": avatarUrl,
        "web_url": webUrl,
      };
}

class Links {
  Links({
    this.self,
    this.notes,
    this.awardEmoji,
    this.project,
  });

  String self;
  String notes;
  String awardEmoji;
  String project;

  factory Links.fromJson(Map<String, dynamic> json) => Links(
        self: json["self"],
        notes: json["notes"],
        awardEmoji: json["award_emoji"],
        project: json["project"],
      );

  Map<String, dynamic> toJson() => {
        "self": self,
        "notes": notes,
        "award_emoji": awardEmoji,
        "project": project,
      };
}

class References {
  References({
    this.short,
    this.relative,
    this.full,
  });

  String short;
  String relative;
  String full;

  factory References.fromJson(Map<String, dynamic> json) => References(
        short: json["short"],
        relative: json["relative"],
        full: json["full"],
      );

  Map<String, dynamic> toJson() => {
        "short": short,
        "relative": relative,
        "full": full,
      };
}

class TaskCompletionStatus {
  TaskCompletionStatus({
    this.count,
    this.completedCount,
  });

  int count;
  int completedCount;

  factory TaskCompletionStatus.fromJson(Map<String, dynamic> json) =>
      TaskCompletionStatus(
        count: json["count"],
        completedCount: json["completed_count"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "completed_count": completedCount,
      };
}

class TimeStats {
  TimeStats({
    this.timeEstimate,
    this.totalTimeSpent,
    this.humanTimeEstimate,
    this.humanTotalTimeSpent,
  });

  int timeEstimate;
  int totalTimeSpent;
  dynamic humanTimeEstimate;
  dynamic humanTotalTimeSpent;

  factory TimeStats.fromJson(Map<String, dynamic> json) => TimeStats(
        timeEstimate: json["time_estimate"],
        totalTimeSpent: json["total_time_spent"],
        humanTimeEstimate: json["human_time_estimate"],
        humanTotalTimeSpent: json["human_total_time_spent"],
      );

  Map<String, dynamic> toJson() => {
        "time_estimate": timeEstimate,
        "total_time_spent": totalTimeSpent,
        "human_time_estimate": humanTimeEstimate,
        "human_total_time_spent": humanTotalTimeSpent,
      };
}

class CreateIssue extends StatefulWidget {
  final String title;
  final String token;
  final String projectId;
  final String label;
  CreateIssue({this.label, this.projectId, this.title, this.token});
  @override
  _CreateIssueState createState() => _CreateIssueState();
}

class _CreateIssueState extends State<CreateIssue> {
  Future<Issue> _createIssue(String token, String projectId, String title,
      String label, String issueDes) async {
    final postsURL =
        'https://gitlab.com/api/v4/projects/$projectId/issues?private_token=$token';
    final response = await http.post(postsURL,
        body: {"title": title, "labels": label, "description": issueDes});
    if (response.statusCode == 201) {
      final String responseString = response.body;
      return issueFromJson(responseString);
    } else {
      return null;
    }
  }

  final TextEditingController issueTitle = TextEditingController();
  final TextEditingController issueLabel = TextEditingController();
  final TextEditingController issueDes = TextEditingController();
  Issue _issue;
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              controller: issueTitle,
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueGrey),
                  ),
                  hintText: "Title",
                  filled: true,
                  fillColor: Colors.grey[200]),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: issueLabel,
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueGrey),
                  ),
                  hintText: "Label ",
                  filled: true,
                  fillColor: Colors.grey[200]),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: issueDes,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueGrey),
                  ),
                  hintText: "Description",
                  filled: true,
                  fillColor: Colors.grey[200]),
            ),
            SizedBox(
              height: 20.0,
            ),
            RaisedButton(
              onPressed: () async {
                final String title = issueTitle.text;
                final String label = issueLabel.text;
                final String issuedes = issueDes.text;
                final Issue issue = await _createIssue(
                    widget.token, widget.projectId, title, label, issuedes);
                setState(
                  () {
                    _issue = issue;
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content:
                              Text("The issue has been created succesfully."),
                          actions: [
                            FlatButton(
                              child: Text(
                                "OK",
                                style: TextStyle(
                                  color: Colors.blueAccent,
                                ),
                              ),
                              onPressed: () {
                                int count = 0;
                                Navigator.popUntil(context, (route) {
                                  return count++ == 2;
                                });
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                );
              },
              child: new Text("Create Issue"),
              color: Colors.green,
              textColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
