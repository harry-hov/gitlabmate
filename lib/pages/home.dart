import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_mate/main.dart';
//import 'package:gitlab_mate/pages/AssignedIssue.dart';
import 'package:gitlab_mate/pages/searchApi.dart';
import 'package:gitlab_mate/pages/starredProjectList.dart';
import 'package:http/http.dart' as http;
import 'package:gitlab_mate/pages/profile.dart';
import 'package:gitlab_mate/pages/userProjectList.dart';
import 'dart:convert';
import 'dart:async';
import 'profile.dart';
import 'package:gitlab_mate/services/storage.dart' as storage;
import 'AssignedIssueList.dart';
import 'MergeRequest.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'issueList.dart';

// To parse this JSON data, do
//
//     final searchProjects = searchProjectsFromJson(jsonString);

class User {
  final String userId;
  final String avatarUrl;
  final String name;
  final String username;

  User({
    this.userId,
    this.avatarUrl,
    this.name,
    this.username,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['id'].toString(),
      avatarUrl: json['avatar_url'].toString(),
      name: json['name'].toString(),
      username: json['username'].toString(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({this.title, this.token});

  final String title;
  final String token;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  _logoutUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SplashScreen()),
        (Route<dynamic> route) => false);
  }

  Future<User> fetchUser(String token) async {
    final response =
        await http.get('https://gitlab.com/api/v4/user?private_token=$token');
    if (response.statusCode == 200) {
      return User.fromJson(jsonDecode(response.body));
    } else {
      //return User(userId: null, avatarUrl: null, name: null, username: null);
      _logoutUser();
    }
  }

  Future<User> futureUser;
  @override
  void initState() {
    super.initState();
    futureUser = fetchUser(widget.token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<User>(
          future: futureUser,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 20.0,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ProfilePage(
                                    token: widget.token,
                                    userAvatar: snapshot.data.avatarUrl,
                                    name: snapshot.data.name,
                                    userName: snapshot.data.username,
                                  ),
                                ),
                              );
                            },
                            child: Hero(
                              tag: "user-avatar",
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(100.0)),
                                child: CachedNetworkImage(
                                  width: 40.0,
                                  height: 40.0,
                                  fit: BoxFit.cover,
                                  imageUrl: snapshot.data.avatarUrl,
                                  placeholder: (context, url) =>
                                      new CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Colors.yellow[700]),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      new Icon(Icons.error),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      FlatButton(
                        textColor: Colors.white,
                        onPressed: () {
                          showSearch(
                              context: context, delegate: Search(widget.token));
                        },
                        child: Icon(
                          Icons.search,
                          color: Colors.orange,
                        ),
                        shape: CircleBorder(
                          side: BorderSide(
                            color: Colors.transparent,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Text(
                      "Home",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.only(left: 20),
                    leading: Text(
                      "Projects",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: Icon(
                            Icons.star,
                            color: Colors.yellow[700],
                          ),
                          title: Text(
                            "Starred",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: Colors.grey[400],
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StarredProjectList(
                                  token: widget.token,
                                  userId: snapshot.data.userId,
                                  title: "Starred Projects",
                                ),
                              ),
                            );
                          },
                        ),
                        Divider(
                          color: Colors.grey[300],
                          endIndent: 0.0,
                          indent: 70.0,
                        ),
                        ListTile(
                          leading: Icon(
                            Icons.my_library_books_sharp,
                            color: Colors.lightBlue,
                          ),
                          title: Text(
                            "Your Projects",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: Colors.grey[400],
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => UserProjectList(
                                  token: widget.token,
                                  userId: snapshot.data.userId,
                                  title: "Your Projects",
                                ),
                              ),
                            );
                          },
                        ),
                        Divider(
                          color: Colors.grey[300],
                          endIndent: 0.0,
                          indent: 70.0,
                        ),
                        ListTile(
                          leading: Icon(
                            Icons.info_outline,
                            color: Colors.blue,
                          ),
                          title: Text(
                            "Issues",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: Colors.grey[400],
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AssignedIssueList(
                                  token: widget.token,
                                  //title: widget.title,
                                  userId: snapshot.data.userId,
                                ),
                              ),
                            );
                          },
                        ),
                        Divider(
                          color: Colors.grey[300],
                          endIndent: 0.0,
                          indent: 70.0,
                        ),
                        ListTile(
                          leading: Icon(
                            Icons.account_tree_outlined,
                            color: Colors.blue,
                          ),
                          title: Text(
                            "Merge Request",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: Colors.grey[400],
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MergeRequest(
                                  token: widget.token,
                                  //title: widget.title,
                                  //projectId: "22053555",
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.only(
                      left: 20,
                      right: 15,
                      bottom: 0,
                    ),
                    leading: Text(
                      "Pinned",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Center(
                            child: Text(
                              "No Pinned Projects :)",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

class Search extends SearchDelegate<User> {
  Search(this.token);
  String token;
  var resultList;
  var result;
  static List searchZone = [false, false, false, false];

  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions
    return null;
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return null;
  }

  @override
  Widget buildResults(BuildContext context) {
    // Todo: implement buildResults
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => searchApi(
                        token: token,
                        Query: query,
                        SearchZone: [false, false, false, true],
                        //issueId: _issueList[index].issueId,
                      ),
                    ),
                  );
                  //_HomePageState().getProject(query, token);
                },
                child: Text("Project with :" + query,
                    style: TextStyle(fontSize: 20))),
          ),
        ),
        Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
                onPressed: () async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => searchApi(
                        token: token,
                        Query: query,
                        SearchZone: [true, false, false, false],
                        //issueId: _issueList[index].issueId,
                      ),
                    ),
                  );
                  //_HomePageState().getIssue(query, token);
                  //print(issueList);
                },
                child: Text(
                  "Issue with :" + query,
                  style: TextStyle(fontSize: 20),
                )),
          ),
        ),
        Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => searchApi(
                        token: token,
                        Query: query,
                        SearchZone: [false, true, false, false],
                        //issueId: _issueList[index].issueId,
                      ),
                    ),
                  );
                  //_HomePageState().getMr(query, token);
                },
                child: Text("Merge request with: " + query,
                    style: TextStyle(fontSize: 20))),
          ),
        ),
        Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => searchApi(
                        token: token,
                        Query: query,
                        SearchZone: [false, false, true, false],
                        //issueId: _issueList[index].issueId,
                      ),
                    ),
                  );
                  // _HomePageState().getUser(query, token);
                },
                child: Text("People with :" + query,
                    style: TextStyle(fontSize: 20))),
          ),
        ),
      ],
    );
  }
}
