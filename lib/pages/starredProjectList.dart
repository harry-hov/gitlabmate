import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:convert';
import 'dart:async';

class Project {
  final String projectId;
  final String projectAvatarUrl;
  final String projectName;
  final String pathWithNamespace;
  final String readmeUrl;

  Project({
    this.projectId,
    this.projectAvatarUrl,
    this.projectName,
    this.pathWithNamespace,
    this.readmeUrl,
  });

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      projectId: json['id'].toString(),
      projectAvatarUrl: json['avatar_url'].toString(),
      projectName: json['name'].toString(),
      pathWithNamespace: json['path_with_namespace'].toString(),
      readmeUrl: json['readme_url'].toString(),
    );
  }
}

class StarredProjectList extends StatefulWidget {
  StarredProjectList({this.title, this.token, this.userId});

  final String title;
  final String token;
  final String userId;

  @override
  _StarredProjectListState createState() => _StarredProjectListState();
}

class _StarredProjectListState extends State<StarredProjectList> {
  List<Project> _projectList = new List<Project>();
  Future<List<Project>> _futureProject;

  Future<List<Project>> fetchProjects(String token, String userId) async {
    final response = await http.get(
        'https://gitlab.com/api/v4/projects?simple=true&starred=true&private_token=$token');

    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      values = json.decode(response.body);
      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _projectList.add(Project.fromJson(map));
          }
        }
      }
      return _projectList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    _futureProject = fetchProjects(widget.token, widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate: ProjectSearch(_projectList, widget.token, false),
                  );
                },
                child: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          FutureBuilder<List<Project>>(
            future: _futureProject,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length > 0) {
                  return Expanded(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (_, int index) {
                          return Container(
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  leading: Hero(
                                    tag: snapshot.data[index].projectId,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.grey.withOpacity(.5),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(100.0)),
                                        child: snapshot.data[index]
                                                    .projectAvatarUrl !=
                                                null
                                            ? CachedNetworkImage(
                                                width: 50.0,
                                                height: 50.0,
                                                fit: BoxFit.cover,
                                                imageUrl: snapshot.data[index]
                                                    .projectAvatarUrl,
                                                placeholder: (context, url) =>
                                                    new CircularProgressIndicator(
                                                  valueColor:
                                                      AlwaysStoppedAnimation(
                                                          Colors.yellow[700]),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Center(
                                                  child: Text(
                                                    snapshot.data[index]
                                                        .projectName[0]
                                                        .toUpperCase(),
                                                    style: TextStyle(
                                                      fontSize: 30.0,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Center(
                                                child: Text(
                                                  snapshot.data[index]
                                                      .projectName[0]
                                                      .toUpperCase(),
                                                  style: TextStyle(
                                                    fontSize: 30.0,
                                                  ),
                                                ),
                                              ),
                                      ),
                                    ),
                                  ),
                                  title: Text(snapshot.data[index].projectName),
                                  subtitle: RichText(
                                    text: TextSpan(
                                      text: '\n',
                                      style: new TextStyle(
                                        fontSize: 5.0,
                                        color: Colors.grey,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: "Project ID: ",
                                          style: TextStyle(
                                            fontSize: 13.0,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        TextSpan(
                                          text: snapshot.data[index].projectId +
                                              "\n",
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: Colors.orange),
                                        ),
                                        TextSpan(
                                          text: "\n",
                                          style: TextStyle(
                                              fontSize: 0.0,
                                              color: Colors.orange),
                                        ),
                                        TextSpan(
                                          text: snapshot
                                              .data[index].pathWithNamespace,
                                          style: TextStyle(
                                            fontSize: 13.0,
                                            color: Colors.grey[400],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 15,
                                    color: Colors.grey[400],
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            ProjectDescription(
                                          token: widget.token,
                                          projectId:
                                              snapshot.data[index].projectId,
                                          projectName:
                                              snapshot.data[index].projectName,
                                          projectAvatar: snapshot
                                              .data[index].projectAvatarUrl,
                                          readmeUrl:
                                              snapshot.data[index].readmeUrl,
                                          isStar: true,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                Divider(
                                  color: Colors.grey[300],
                                  endIndent: 0.0,
                                  indent: 80.0,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  );
                } else {
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Center(
                            child: Text(
                              "No Starred Projects.",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  );
                }
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                margin: EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Center(
                        child: CircularProgressIndicator(),
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

class ProjectSearch extends SearchDelegate<Project> {
  final List<Project> projectList;
  static Project result;
  final String token;
  bool flag;
  ProjectSearch(this.projectList, this.token, this.flag);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? projectList
        : projectList
            .where((p) =>
                p.projectName.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No results found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                onTap: () {
                  result = showList;
                  query = showList.projectName;

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProjectDescription(
                        token: token,
                        projectId: showList.projectId,
                        projectName: showList.projectName,
                        projectAvatar: showList.projectAvatarUrl,
                        readmeUrl: showList.readmeUrl,
                        isStar: true,
                      ),
                    ),
                  );

                  //showResults(context);
                },
                title: Text(
                  showList.projectName,
                ),
                subtitle: RichText(
                  text: TextSpan(
                    text: '\n',
                    style: new TextStyle(
                      fontSize: 5.0,
                      color: Colors.grey,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "#" + showList.projectId + "\n",
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.blue,
                        ),
                      ),
                      TextSpan(
                        text: "\n",
                        style: TextStyle(fontSize: 0.0, color: Colors.orange),
                      ),
                      TextSpan(
                        text: "",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey,
                        ),
                      ),
                      TextSpan(
                        text: showList.pathWithNamespace + "\n",
                        style: TextStyle(fontSize: 12.5, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                /*
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      showList.projectName,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      showList.pathWithNamespace,
                      style: TextStyle(color: Colors.grey),
                    ),
                    Divider(),
                  ],
                ),
                */
              );
            },
          );
  }
}
