import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Notes notesFromJson(String str) => Notes.fromJson(json.decode(str));

String notesToJson(Notes data) => json.encode(data.toJson());

class Notes {
  int id;
  Null type;
  String body;
  Null attachment;
  Author author;
  String createdAt;
  String updatedAt;
  bool system;
  int noteableId;
  String noteableType;
  bool resolvable;
  bool confidential;
  int noteableIid;
  CommandsChanges commandsChanges;

  Notes(
      {this.id,
      this.type,
      this.body,
      this.attachment,
      this.author,
      this.createdAt,
      this.updatedAt,
      this.system,
      this.noteableId,
      this.noteableType,
      this.resolvable,
      this.confidential,
      this.noteableIid,
      this.commandsChanges});

  Notes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    body = json['body'];
    attachment = json['attachment'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    system = json['system'];
    noteableId = json['noteable_id'];
    noteableType = json['noteable_type'];
    resolvable = json['resolvable'];
    confidential = json['confidential'];
    noteableIid = json['noteable_iid'];
    commandsChanges = json['commands_changes'] != null
        ? new CommandsChanges.fromJson(json['commands_changes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['body'] = this.body;
    data['attachment'] = this.attachment;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['system'] = this.system;
    data['noteable_id'] = this.noteableId;
    data['noteable_type'] = this.noteableType;
    data['resolvable'] = this.resolvable;
    data['confidential'] = this.confidential;
    data['noteable_iid'] = this.noteableIid;
    if (this.commandsChanges != null) {
      data['commands_changes'] = this.commandsChanges.toJson();
    }
    return data;
  }
}

class Author {
  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  Author(
      {this.id,
      this.name,
      this.username,
      this.state,
      this.avatarUrl,
      this.webUrl});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['state'] = this.state;
    data['avatar_url'] = this.avatarUrl;
    data['web_url'] = this.webUrl;
    return data;
  }
}

class CommandsChanges {
  //CommandsChanges({});

  CommandsChanges.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class CreateNote extends StatefulWidget {
  //final String title;
  final String token;
  final String projectId;
  //final String ;

  final String issueId;
  CreateNote({this.projectId, this.token, this.issueId});
  @override
  _CreateNoteState createState() => _CreateNoteState();
}

class _CreateNoteState extends State<CreateNote> {
  Future<Notes> _createIssue(
      String token, String projectId, String body, String issueId) async {
    final postsURL =
        'https://gitlab.com/api/v4/projects/$projectId/issues/$issueId/notes?&private_token=$token';
    final response = await http.post(postsURL, body: {"body": body});
    if (response.statusCode == 201) {
      final String responseString = response.body;
      return notesFromJson(responseString);
    } else {
      return null;
    }
  }

  final TextEditingController noteBody = TextEditingController();
  //final TextEditingController issueLabel = TextEditingController();
  Notes _notes;
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      child: Text(
                        "Add Comment",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              controller: noteBody,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueGrey),
                  ),
                  hintText: "Type your comment here",
                  filled: true,
                  fillColor: Colors.grey[200]),
            ),
            SizedBox(
              height: 32,
            ),
            RaisedButton(
              color: Colors.green,
              textColor: Colors.white,
              child: new Text("Add Comment"),
              onPressed: () async {
                final String body = noteBody.text;
                // final String label = issueLabel.text;
                final Notes issue = await _createIssue(
                    widget.token, widget.projectId, body, widget.issueId);
                setState(() {
                  _notes = issue;
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: Text("Comment has been added succesfully."),
                        actions: [
                          FlatButton(
                            child: Text(
                              "OK",
                              style: TextStyle(
                                color: Colors.blueAccent,
                              ),
                            ),
                            onPressed: () {
                              int count = 0;
                              Navigator.popUntil(context, (route) {
                                return count++ == 2;
                              });
                            },
                          ),
                        ],
                      );
                    },
                  );
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
