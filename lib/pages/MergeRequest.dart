import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/mergeRequestDescription.dart';
import 'package:http/http.dart' as http;

// To parse this JSON data, do
//
//     final mr = mrFromJson(jsonString);

import 'dart:convert';

import 'IssueDescription.dart';

List<Mr> mrFromJson(String str) =>
    List<Mr>.from(json.decode(str).map((x) => Mr.fromJson(x)));

String mrToJson(List<Mr> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Mr {
  Mr({
    this.id,
    this.iid,
    this.projectId,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.mergedBy,
    this.mergedAt,
    this.closedBy,
    this.closedAt,
    this.targetBranch,
    this.sourceBranch,
    this.userNotesCount,
    this.upvotes,
    this.downvotes,
    this.author,
    this.assignees,
    this.assignee,
    this.reviewers,
    this.sourceProjectId,
    this.targetProjectId,
    this.labels,
    this.workInProgress,
    this.milestone,
    this.mergeWhenPipelineSucceeds,
    this.mergeStatus,
    this.sha,
    this.mergeCommitSha,
    this.squashCommitSha,
    this.discussionLocked,
    this.shouldRemoveSourceBranch,
    this.forceRemoveSourceBranch,
    this.allowCollaboration,
    this.allowMaintainerToPush,
    this.reference,
    this.references,
    this.webUrl,
    this.timeStats,
    this.squash,
    this.taskCompletionStatus,
    this.hasConflicts,
    this.blockingDiscussionsResolved,
    this.approvalsBeforeMerge,
  });

  int id;
  int iid;
  int projectId;
  String title;
  String description;
  String state;
  DateTime createdAt;
  DateTime updatedAt;
  Author mergedBy;
  DateTime mergedAt;
  dynamic closedBy;
  dynamic closedAt;
  String targetBranch;
  String sourceBranch;
  int userNotesCount;
  int upvotes;
  int downvotes;
  Author author;
  List<dynamic> assignees;
  dynamic assignee;
  List<Author> reviewers;
  int sourceProjectId;
  int targetProjectId;
  List<dynamic> labels;
  bool workInProgress;
  dynamic milestone;
  bool mergeWhenPipelineSucceeds;
  String mergeStatus;
  String sha;
  String mergeCommitSha;
  dynamic squashCommitSha;
  dynamic discussionLocked;
  dynamic shouldRemoveSourceBranch;
  bool forceRemoveSourceBranch;
  bool allowCollaboration;
  bool allowMaintainerToPush;
  String reference;
  References references;
  String webUrl;
  TimeStats timeStats;
  bool squash;
  TaskCompletionStatus taskCompletionStatus;
  bool hasConflicts;
  bool blockingDiscussionsResolved;
  dynamic approvalsBeforeMerge;

  factory Mr.fromJson(Map<String, dynamic> json) => Mr(
        id: json["id"],
        iid: json["iid"],
        projectId: json["project_id"],
        title: json["title"],
        description: json["description"],
        state: json["state"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        mergedBy: Author.fromJson(json["merged_by"]),
        mergedAt: DateTime.parse(json["merged_at"]),
        closedBy: json["closed_by"],
        closedAt: json["closed_at"],
        targetBranch: json["target_branch"],
        sourceBranch: json["source_branch"],
        userNotesCount: json["user_notes_count"],
        upvotes: json["upvotes"],
        downvotes: json["downvotes"],
        author: Author.fromJson(json["author"]),
        assignees: List<dynamic>.from(json["assignees"].map((x) => x)),
        assignee: json["assignee"],
        reviewers:
            List<Author>.from(json["reviewers"].map((x) => Author.fromJson(x))),
        sourceProjectId: json["source_project_id"],
        targetProjectId: json["target_project_id"],
        labels: List<dynamic>.from(json["labels"].map((x) => x)),
        workInProgress: json["work_in_progress"],
        milestone: json["milestone"],
        mergeWhenPipelineSucceeds: json["merge_when_pipeline_succeeds"],
        mergeStatus: json["merge_status"],
        sha: json["sha"],
        mergeCommitSha: json["merge_commit_sha"],
        squashCommitSha: json["squash_commit_sha"],
        discussionLocked: json["discussion_locked"],
        shouldRemoveSourceBranch: json["should_remove_source_branch"],
        forceRemoveSourceBranch: json["force_remove_source_branch"],
        allowCollaboration: json["allow_collaboration"],
        allowMaintainerToPush: json["allow_maintainer_to_push"],
        reference: json["reference"],
        references: References.fromJson(json["references"]),
        webUrl: json["web_url"],
        timeStats: TimeStats.fromJson(json["time_stats"]),
        squash: json["squash"],
        taskCompletionStatus:
            TaskCompletionStatus.fromJson(json["task_completion_status"]),
        hasConflicts: json["has_conflicts"],
        blockingDiscussionsResolved: json["blocking_discussions_resolved"],
        approvalsBeforeMerge: json["approvals_before_merge"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "iid": iid,
        "project_id": projectId,
        "title": title,
        "description": description,
        "state": state,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "merged_by": mergedBy.toJson(),
        "merged_at": mergedAt.toIso8601String(),
        "closed_by": closedBy,
        "closed_at": closedAt,
        "target_branch": targetBranch,
        "source_branch": sourceBranch,
        "user_notes_count": userNotesCount,
        "upvotes": upvotes,
        "downvotes": downvotes,
        "author": author.toJson(),
        "assignees": List<dynamic>.from(assignees.map((x) => x)),
        "assignee": assignee,
        "reviewers": List<dynamic>.from(reviewers.map((x) => x.toJson())),
        "source_project_id": sourceProjectId,
        "target_project_id": targetProjectId,
        "labels": List<dynamic>.from(labels.map((x) => x)),
        "work_in_progress": workInProgress,
        "milestone": milestone,
        "merge_when_pipeline_succeeds": mergeWhenPipelineSucceeds,
        "merge_status": mergeStatus,
        "sha": sha,
        "merge_commit_sha": mergeCommitSha,
        "squash_commit_sha": squashCommitSha,
        "discussion_locked": discussionLocked,
        "should_remove_source_branch": shouldRemoveSourceBranch,
        "force_remove_source_branch": forceRemoveSourceBranch,
        "allow_collaboration": allowCollaboration,
        "allow_maintainer_to_push": allowMaintainerToPush,
        "reference": reference,
        "references": references.toJson(),
        "web_url": webUrl,
        "time_stats": timeStats.toJson(),
        "squash": squash,
        "task_completion_status": taskCompletionStatus.toJson(),
        "has_conflicts": hasConflicts,
        "blocking_discussions_resolved": blockingDiscussionsResolved,
        "approvals_before_merge": approvalsBeforeMerge,
      };
}

class Author {
  Author({
    this.id,
    this.name,
    this.username,
    this.state,
    this.avatarUrl,
    this.webUrl,
  });

  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  factory Author.fromJson(Map<String, dynamic> json) => Author(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        state: json["state"],
        avatarUrl: json["avatar_url"],
        webUrl: json["web_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "username": username,
        "state": state,
        "avatar_url": avatarUrl,
        "web_url": webUrl,
      };
}

class References {
  References({
    this.short,
    this.relative,
    this.full,
  });

  String short;
  String relative;
  String full;

  factory References.fromJson(Map<String, dynamic> json) => References(
        short: json["short"],
        relative: json["relative"],
        full: json["full"],
      );

  Map<String, dynamic> toJson() => {
        "short": short,
        "relative": relative,
        "full": full,
      };
}

class TaskCompletionStatus {
  TaskCompletionStatus({
    this.count,
    this.completedCount,
  });

  int count;
  int completedCount;

  factory TaskCompletionStatus.fromJson(Map<String, dynamic> json) =>
      TaskCompletionStatus(
        count: json["count"],
        completedCount: json["completed_count"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "completed_count": completedCount,
      };
}

class TimeStats {
  TimeStats({
    this.timeEstimate,
    this.totalTimeSpent,
    this.humanTimeEstimate,
    this.humanTotalTimeSpent,
  });

  int timeEstimate;
  int totalTimeSpent;
  dynamic humanTimeEstimate;
  dynamic humanTotalTimeSpent;

  factory TimeStats.fromJson(Map<String, dynamic> json) => TimeStats(
        timeEstimate: json["time_estimate"],
        totalTimeSpent: json["total_time_spent"],
        humanTimeEstimate: json["human_time_estimate"],
        humanTotalTimeSpent: json["human_total_time_spent"],
      );

  Map<String, dynamic> toJson() => {
        "time_estimate": timeEstimate,
        "total_time_spent": totalTimeSpent,
        "human_time_estimate": humanTimeEstimate,
        "human_total_time_spent": humanTotalTimeSpent,
      };
}

class MergeRequest extends StatefulWidget {
  final String token;

  const MergeRequest({this.token});

  @override
  _MergeRequestState createState() => _MergeRequestState();
}

class _MergeRequestState extends State<MergeRequest> {
  @override
  List<Mr> _mrList = new List<Mr>();
  Future<List<Mr>> _fetchMrRequest(String token) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/merge_requests?state=all&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _mrList.add(Mr.fromJson(map));
          }
        }
      }
      return _mrList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchMrRequest(widget.token);
    _mrList.sort();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        margin: EdgeInsets.all(8),
        child: new Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 18.0,
                    ),
                    Container(
                      child: Text(
                        "Merge Requests",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 28.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                FlatButton(
                  textColor: Colors.white,
                  onPressed: () {
                    showSearch(
                      context: context,
                      delegate: MrSearch(_mrList, widget.token),
                    );
                  },
                  child: Icon(
                    Icons.search,
                    color: Colors.orange,
                  ),
                  shape: CircleBorder(
                    side: BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
                ),
              ],
            ),
            createListView(_mrList),
          ],
        ),
      ),
    );
  }

  Widget createListView(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  title: RichText(
                    text: TextSpan(
                      text: listVariable[index].title,
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text:
                              " (!" + listVariable[index].iid.toString() + ")",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                  ),
                  subtitle: RichText(
                    text: TextSpan(
                      text: '\n',
                      style: new TextStyle(
                        fontSize: 5.0,
                        color: Colors.grey,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Status: ",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: listVariable[index].state.toUpperCase() + "\n",
                          style: TextStyle(fontSize: 12.5, color: Colors.green),
                        ),
                      ],
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                    color: Colors.grey[400],
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MergeRequestDescription(
                          token: widget.token,
                          mrId: listVariable[index].id.toString(),
                          mrIid: listVariable[index].iid.toString(),
                          title: listVariable[index].title,
                          mrDescription: listVariable[index].description,
                          mrState: listVariable[index].state,
                          projectId: listVariable[index].projectId.toString(),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class MrSearch extends SearchDelegate<Issue> {
  final List<Mr> mrList;
  static Mr result;
  final String token;
  MrSearch(this.mrList, this.token);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? mrList
        : mrList
            .where((p) => p.title.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No results found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                onTap: () {
                  result = showList;
                  query = showList.title;
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => IssueDescription(
                        token: token,
                        issueId: showList.iid.toString(),
                        //projectId: result.projectId,
                      ),
                    ),
                  );
                  //showResults(context);
                },
                title: Text(showList.title),
                subtitle: RichText(
                  text: TextSpan(
                    text: '\n',
                    style: new TextStyle(
                      fontSize: 5.0,
                      color: Colors.grey,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "!" + showList.iid.toString() + "\n",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      TextSpan(
                        text: "\n",
                        style: TextStyle(fontSize: 0.0, color: Colors.orange),
                      ),
                      TextSpan(
                        text: "Status: ",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey,
                        ),
                      ),
                      TextSpan(
                        text: showList.state.toUpperCase() + "\n",
                        style: TextStyle(fontSize: 12.5, color: Colors.green),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }
}
