import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/theme_map.dart';
import 'dart:async';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewCode extends StatefulWidget {
  ViewCode({this.title, this.token, this.name, this.projectId, this.id});

  final String title;
  final String token;
  final String name;
  final String projectId;
  final String id;

  @override
  _ViewCodeState createState() => _ViewCodeState();
}

class _ViewCodeState extends State<ViewCode> {
  String language;
  String theme = 'xcode';
  String fileContent = " ";

  Future<String> _futureFileContent;

  Future<String> fetchFileContent(
      String token, String projectId, String sha) async {
    final response = await http.get(
        'https://gitlab.com/api/v4/projects/$projectId/repository/blobs/$sha/raw?private_token=$token');

    if (response.statusCode == 200) {
      return response.body.toString();
    } else {
      throw Exception('Failed to load File Content');
    }
  }

  @override
  void initState() {
    super.initState();
    _futureFileContent =
        fetchFileContent(widget.token, widget.projectId, widget.id);
    var pos = widget.name.lastIndexOf('.');
    language = (pos != -1)
        ? widget.name.substring(pos + 1, widget.name.length)
        : "null";
  }

  Widget _buildMenuContent(String text) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(children: <Widget>[
        Text(text, style: TextStyle(fontSize: 16)),
        Icon(Icons.arrow_drop_down)
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 30.0,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Expanded(
                    child: Container(
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 23.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  PopupMenuButton<String>(
                    child: _buildMenuContent(theme),
                    itemBuilder: (context) {
                      return themeMap.keys.map((key) {
                        return CheckedPopupMenuItem(
                          value: key,
                          child: Text(key),
                          checked: theme == key,
                        );
                      }).toList();
                    },
                    onSelected: (selected) {
                      if (selected != null) {
                        setState(() {
                          theme = selected;
                        });
                      }
                    },
                  ),
                ],
              ),
              /*
              PopupMenuButton<String>(
                child: _buildMenuContent(theme),
                itemBuilder: (context) {
                  return themeMap.keys.map((key) {
                    return CheckedPopupMenuItem(
                      value: key,
                      child: Text(key),
                      checked: theme == key,
                    );
                  }).toList();
                },
                onSelected: (selected) {
                  if (selected != null) {
                    setState(() {
                      theme = selected;
                    });
                  }
                },
              ),
              */
              Container(
                child: FutureBuilder<String>(
                  future: _futureFileContent,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        margin: EdgeInsets.all(15.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 15.0,
                            ),
                            ListTile(
                              title: HighlightView(
                                snapshot.data,
                                language: language,
                                theme: themeMap[theme],
                                padding: EdgeInsets.all(12),
                                textStyle: TextStyle(
                                  fontFamily: 'RobotoMono',
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                          ],
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: CircularProgressIndicator(),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              /* Logout Button */
            ],
          ),
        ),
      ),
    );
  }
}
