import 'package:flutter/material.dart';
import 'package:gitlab_mate/pages/CreateIssue.dart';
//import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'IssueDescription.dart';

class Issue {
  //final String issueId;
  final String iid;
  final String projectId;
  final String issueTitle;
  final String issueDescription;
  final String issueState;
  final String projectAvatarUrl;
  final Author author;
  final String userNotesCount;
  Issue({
    this.issueDescription,
    this.iid,
    this.issueState,
    this.issueTitle,
    this.projectId,
    this.projectAvatarUrl,
    this.author,
    this.userNotesCount,
  });

  factory Issue.fromJson(Map<String, dynamic> json) {
    return Issue(
      projectId: json['project_id'].toString(),
      iid: json['iid'].toString(),
      author:
          json['author'] != null ? new Author.fromJson(json['author']) : null,
      issueDescription: json['description'].toString(),
      issueState: json['state'].toString(),
      issueTitle: json['title'].toString(),
      projectAvatarUrl: json['author.avatar_url'].toString(),
      userNotesCount: json['user_notes_count'].toString(),
    );
  }
}

class Author {
  int id;
  String name;
  String username;
  String state;
  String avatarUrl;
  String webUrl;

  Author(
      {this.id,
      this.name,
      this.username,
      this.state,
      this.avatarUrl,
      this.webUrl});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    state = json['state'];
    avatarUrl = json['avatar_url'];
    webUrl = json['web_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['state'] = this.state;
    data['avatar_url'] = this.avatarUrl;
    data['web_url'] = this.webUrl;
    return data;
  }
}

class ProjectIssue extends StatefulWidget {
  final String token;
  final String projectId;
  final String title;

  ProjectIssue({this.token, this.projectId, this.title});
  @override
  _ProjectIssueState createState() => _ProjectIssueState();
}

class _ProjectIssueState extends State<ProjectIssue> {
  List<Issue> _projectIssueList = new List<Issue>();
  Future<List<Issue>> _fetchProjectIssue(String token, String projectId) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/projects/$projectId/issues?private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _projectIssueList.add(Issue.fromJson(map));
          }
        }
      }
      return _projectIssueList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchProjectIssue(widget.token, widget.projectId);
    _projectIssueList.sort();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 18.0,
                  ),
                  Container(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate:
                        ProjectIssueSearch(_projectIssueList, widget.token),
                  );
                },
                child: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.add),
                  title: Text(
                    "Create new issue!",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CreateIssue(
                          token: widget.token,
                          title: widget.title,
                          projectId: widget.projectId,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          createListView(_projectIssueList),
        ],
      ),
    );
  }

  Widget createListView(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                ListTile(
                  title: RichText(
                    text: TextSpan(
                      text: listVariable[index].issueTitle,
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: " (#" + listVariable[index].iid + ")",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                  ),
                  subtitle: RichText(
                    text: TextSpan(
                      text: '\n',
                      style: new TextStyle(
                        fontSize: 5.0,
                        color: Colors.grey,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Created by: " +
                              listVariable[index].author.name +
                              "\n",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[500],
                          ),
                        ),
                        TextSpan(
                          text: "\n",
                          style: TextStyle(fontSize: 0.0, color: Colors.orange),
                        ),
                        TextSpan(
                          text: "Status: ",
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: listVariable[index].issueState.toUpperCase() +
                              "\n",
                          style: TextStyle(fontSize: 12.5, color: Colors.green),
                        ),
                      ],
                    ),
                  ),
                  trailing: Text(
                    listVariable[index].userNotesCount,
                    style: TextStyle(
                      color: Colors.blueGrey,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => IssueDescription(
                          token: widget.token,
                          issueId: _projectIssueList[index].iid,
                          title: _projectIssueList[index].issueTitle,
                          issueDescription:
                              _projectIssueList[index].issueDescription,
                          issueState: _projectIssueList[index].issueState,
                          projectId: _projectIssueList[index].projectId,
                          //issueId: _issueList[index].issueId,
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ProjectIssueSearch extends SearchDelegate<Issue> {
  final List<Issue> projectIssueList;
  static Issue result;
  final String token;
  ProjectIssueSearch(this.projectIssueList, this.token);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? projectIssueList
        : projectIssueList
            .where((p) =>
                p.issueTitle.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Card(
            child: ListTile(
              title: Center(
                child: Text("No results found..."),
              ),
            ),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                onTap: () {
                  result = showList;
                  query = showList.issueTitle;
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => IssueDescription(
                        token: token,
                        issueId: showList.iid,
                        //projectId: result.projectId,
                        title: showList.issueTitle,
                        issueDescription: showList.issueDescription,
                        issueState: showList.issueState,
                      ),
                    ),
                  );
                  //showResults(context);
                },
                title: Text(showList.issueTitle),
                subtitle: RichText(
                  text: TextSpan(
                    text: '\n',
                    style: new TextStyle(
                      fontSize: 5.0,
                      color: Colors.grey,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "#" + showList.iid + "\n",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      TextSpan(
                        text: "\n",
                        style: TextStyle(fontSize: 0.0, color: Colors.orange),
                      ),
                      TextSpan(
                        text: "Status: ",
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey,
                        ),
                      ),
                      TextSpan(
                        text: showList.issueState.toUpperCase() + "\n",
                        style: TextStyle(fontSize: 12.5, color: Colors.green),
                      ),
                    ],
                  ),
                ),
                /*
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      showList.issueTitle,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      showList.issueState,
                      style: TextStyle(color: Colors.grey),
                    ),
                    Divider(),
                  ],
                ),
                */
              );
            },
          );
  }
}
