import 'package:flutter/material.dart';
//import 'package:gitlab_mate/pages/projectDescription.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'IssueDescription.dart';

class Issue {
  final String issueId;
  final String projectId;
  final String issueTitle;
  final String issueDescription;
  final String issueState;
  final String projectAvatarUrl;
  Issue(
      {this.issueDescription,
      this.issueId,
      this.issueState,
      this.issueTitle,
      this.projectId,
      this.projectAvatarUrl});

  factory Issue.fromJson(Map<String, dynamic> json) {
    return Issue(
        projectId: json['project_id'].toString(),
        issueId: json['id'].toString(),
        issueDescription: json['description'].toString(),
        issueState: json['state'].toString(),
        issueTitle: json['title'].toString(),
        projectAvatarUrl: json['author.avatar_url'].toString());
  }
}

class OpnedIssueList extends StatefulWidget {
  final String token;
  final String projectId;
  //final String projectId;
  OpnedIssueList({this.token, this.projectId});
  @override
  _OpnedIssueListState createState() => _OpnedIssueListState();
}

class _OpnedIssueListState extends State<OpnedIssueList> {
  List<Issue> _issueList = new List<Issue>();
  Future<List<Issue>> _fetchIssue(String token, String projectId) async {
    final jobsListAPIUrl =
        'https://gitlab.com/api/v4/projects/$projectId/issues?state=opened&private_token=$token';
    final response = await http.get(jobsListAPIUrl);
    if (response.statusCode == 200) {
      List<dynamic> values = new List<dynamic>();
      setState(() {
        values = json.decode(response.body);
      });

      if (values.length > 0) {
        for (int i = 0; i < values.length; i++) {
          if (values[i] != null) {
            Map<String, dynamic> map = values[i];
            _issueList.add(Issue.fromJson(map));
          }
        }
      }
      return _issueList;
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchIssue(widget.token, widget.projectId);
    _issueList.sort();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Issues"),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                showSearch(
                    context: context,
                    delegate: IssueSearch(_issueList, widget.token));
              },
              icon: Icon(Icons.search),
            )
          ],
        ),
        body: new Container(
            margin: EdgeInsets.all(8),
            child: new Column(
              children: <Widget>[createListView(_issueList)],
            )));
  }

  Widget createListView(var listVariable) {
    return Flexible(
      child: new ListView.builder(
        itemCount: listVariable.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(15.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(listVariable[index].issueState),
                    subtitle: RichText(
                      text: TextSpan(
                        text: '\n',
                        style: new TextStyle(
                          fontSize: 5.0,
                          color: Colors.grey,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: "Project ID: ",
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey,
                            ),
                          ),
                          TextSpan(
                            text: listVariable[index].projectId + "\n",
                            style:
                                TextStyle(fontSize: 12.5, color: Colors.orange),
                          ),
                          TextSpan(
                            text: "\n",
                            style:
                                TextStyle(fontSize: 0.0, color: Colors.orange),
                          ),
                          TextSpan(
                            text: listVariable[index].issueDescription,
                            style: TextStyle(
                              fontSize: 13.0,
                              color: Colors.grey[400],
                            ),
                          ),
                        ],
                      ),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 15,
                      color: Colors.grey[400],
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => IssueDescription(
                            token: widget.token,
                            issueId: _issueList[index].issueId,
                            //issueId: _issueList[index].issueId,
                            title: _issueList[index].issueTitle,
                            issueDescription:
                                _issueList[index].issueDescription,
                            issueState: _issueList[index].issueState,
                            projectId: _issueList[index].projectId,
                          ),
                        ),
                      );
                    },
                  ),
                  Divider(
                    color: Colors.grey[300],
                    endIndent: 0.0,
                    indent: 80.0,
                  )
                ],
              ));
        },
      ),
    );
  }
}

class IssueSearch extends SearchDelegate<Issue> {
  final List<Issue> issueList;
  static Issue result;
  final String token;
  IssueSearch(this.issueList, this.token);
  @override
  List<Widget> buildActions(BuildContext context) {
    // Todo: implement buildActions

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Todo: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return null;
  }

  //Add the search term to the searchBloc.
  //The Bloc will then handle the searching and add the results to the searchResults stream.
  //This is the equivalent of submitting the search term to whatever search service you are using

  @override
  Widget buildSuggestions(BuildContext context) {
    // Todo: implement buildSuggestions

    var list = query.isEmpty
        ? issueList
        : issueList
            .where((p) =>
                p.issueTitle.toLowerCase().startsWith(query.toLowerCase()))
            .toList();
    return list.isEmpty
        ? Text(
            "No result fouind....",
            style: TextStyle(fontSize: 20),
          )
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              final showList = list[index];
              return ListTile(
                  onTap: () {
                    result = showList;
                    query = showList.issueTitle;
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => IssueDescription(
                          token: token,
                          issueId: showList.issueId,
                          title: showList.issueTitle,
                          issueDescription: showList.issueDescription,
                          issueState: showList.issueState,
                          //projectId: result.projectId,
                        ),
                      ),
                    );
                    //showResults(context);
                  },
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        showList.issueTitle,
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        showList.issueState,
                        style: TextStyle(color: Colors.grey),
                      ),
                      Divider(),
                    ],
                  ));
            },
          );
  }
}
